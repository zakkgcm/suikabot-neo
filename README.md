# Suikabot-Neo
Multi-protocol, plugin-based chatbot. The fourth incarnation of this bot.

## Dependencies
See requirements.txt, but generally:

* python3
* discord.py
