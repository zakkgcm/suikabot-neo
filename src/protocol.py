import abc
import discord
import asyncio

import events

from common import User


class Protocol(metaclass=abc.ABCMeta):
    '''
    Defines high-level abstract interface for interacting with a chat backend
    '''
    def __init__(self):
        self._event_queue = asyncio.Queue()

    @abc.abstractmethod
    async def connect(self):
        pass

    @abc.abstractmethod
    async def disconnect(self):
        pass

    @abc.abstractmethod
    async def send(self, target):
        pass

    async def put_event(self, event):
        self._event_queue.put_nowait(event)

    async def pop_event(self):
        return await self._event_queue.get()


class DiscordProtocol(Protocol):
    '''
    Discord protocol backend, implements Protocol
    '''
    def __init__(self, token):
        super().__init__()
        self._token = token
        self._dsclient = self.DiscordClientWrapper(self)

    async def connect(self):
        print("Connecting to Discord...")
        await self._dsclient.start(self._token)

    async def disconnect(self):
        await self._dsclient.logout()

    async def send(self, target, message):
        if target.channel:
            channel = self._dsclient.get_channel(target.channel)
            await channel.send(message)

    class DiscordClientWrapper(discord.Client):
        '''
        Inner discord.Client wrapper to receive events
        '''
        def __init__(self, proto):
            self._proto = proto
            super().__init__()

        async def on_connect(self):
            print("connected!")

        async def on_ready(self):
            print("Suika online!")

        async def on_message(self, message):
            if message.author == self.user:
                return

            author = message.author.name + '#' + message.author.discriminator
            sender = User(message.author.id, author, {'registered', False})

            await self._proto.put_event(events.MessageEvent(
                sender, message.channel.id,
                message.content,
                raw=message))
