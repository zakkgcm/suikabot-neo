from common import Target
from plugin import Plugin

p = Plugin()

AUTH_USER = 1
AUTH_ADMIN = 9001


@p.event(event_type='message')
async def on_goblin(client, event):
    if event.content.startswith('goblin'):
        await client.send(
            Target(channel=event.channel),
            'japanese goblin!'
        )
        return True, event

    if event.sender.attr('auth_level', default=AUTH_USER) >= AUTH_ADMIN:
        if event.content.startswith('!reload'):
            client.reload()
            await client.send(
                Target(channel=event.channel),
                'plugins reloaded.'
            )
            return False, event
        elif event.content.startswith('!adduser'):
            _, username = event.content.split()
            client.users.add_user(username, '{}')
            await client.send(
                Target(channel=event.channel),
                f'Added user {username} to database.'
            )
        elif event.content.startswith('!alias'):
            _, uid, alias = event.content.split()
            client.users.add_alias(alias, uid)

            await client.send(
                Target(channel=event.channel),
                f'Added alias {alias} for user {uid}.'
            )

    return True, event


@p.event(event_type='message')
async def on_message(client, event):
    message = event.raw
    print(message)
    print(message.content)
    print(event.sender.id, event.sender.name)

    return True, event

