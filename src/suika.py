#!/usr/bin/env python

import os
import asyncio

import logging

from collections import defaultdict
from dotenv import load_dotenv

from plugin import PluginManager
from protocol import DiscordProtocol
from events import Event

from common import UserManager

log = logging.getLogger('suika')


class SuikaClient:
    '''
        Main bot class
        Interface between client protocol APIs and plugins.
        Dispatches events to loaded plugins and
        routes commands to protocols
    '''

    def __init__(self, protocol, plugins=None):
        self._protocol = protocol
        self.users = UserManager('users.db')
        self.plugins = plugins

    def run(self):
        # FIXME: the client should NOT handle event loop
        # do this outside
        loop = asyncio.get_event_loop()
        loop. create_task(self._dispatch_to_plugins())

        try:
            loop.run_until_complete(self._protocol.connect())
        except KeyboardInterrupt:
            loop.run_until_complete(self._protocol.disconnect())
        finally:
            loop.close()

    async def _dispatch_to_plugins(self):
        while True:
            ev = await self._protocol.pop_event()
            ev.on_dispatch(client)

            await self.plugins.dispatch(self, ev)

    async def reload(self, name=None):
        if name is None:  # reload all
            self.plugins.dispatch(self, Event('_cleanup'))
            self.plugins.reload_all()
        else:  # reload specified plugin
            self.plugins.dispatch_one(self, Event('_cleanup'), name)
            self.plugins.reload(name)

    async def send(self, target, message):
        await self._protocol.send(target, message)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s :: %(levelname)s :: %(filename)s:%(lineno)d :: %(message)s')

    load_dotenv()

    discord_proto = DiscordProtocol(os.getenv('DISCORD_TOKEN'))

    plugins = PluginManager()
    plugins.load_all()

    client = SuikaClient(discord_proto, plugins)
    client.run()
