import logging

import sqlite3
import json


log = logging.getLogger('suika.common')


# Users
class User:
    def __init__(self, uid, name, attrs={}):
        self.id = uid
        self.name = name
        self._attrs = attrs

    def attr(self, key, default=None):
        if key in self._attrs:
            return self._attrs[key]

        return None


class UserManager():
    def __init__(self, dbfile):
        self._dbfile = dbfile
        self._db = sqlite3.connect(self._dbfile)
        self._create_db()

    def _create_db(self):
        c = self._db.cursor()
        try:
            c.execute('''
                CREATE TABLE IF NOT EXISTS users(
                    id          INTEGER NOT NULL PRIMARY KEY,
                    name        TEXT NOT NULL,
                    preferences TEXT
                )
            ''')
            c.execute('''
                CREATE TABLE IF NOT EXISTS aliases (
                    id      INTEGER NOT NULL PRIMARY KEY,
                    alias   TEXT NOT NULL UNIQUE,
                    user_id INTEGER,
                    FOREIGN KEY(user_id) REFERENCES users(id)
                )
            ''')

        except Exception as e:
            log.error(e)

    def get_user(self, key, default=None):
        c = self._db.cursor()
        try:
            c.execute('''
                SELECT users.id, users.name, users.preferences
                FROM users INNER JOIN aliases ON aliases.alias == ?
            ''', [key])

            uid, name, pref_json = c.fetchone()

            attrs = {}
            try:
                attrs = json.loads(pref_json)
            except json.decoder.JSONDecodeError as e:
                log.error(e)

            return User(uid, name, attrs=attrs)

        except Exception as e:
            log.error(e)

    def add_user(self, name, attrs):
        c = self._db.cursor()
        try:
            c.execute('''
                INSERT INTO users (name, preferences) VALUES (?, ?)
            ''', (name, json.dumps(attrs)))
            self._db.commit()

        except Exception as e:
            log.error(e)

    def add_alias(self, uid, alias):
        c = self._db.cursor()
        try:
            c.execute('''
                INSERT INTO aliases (alias, user_id) VALUES (?, ?)
            ''', (alias, uid))
            self._db.commit()

        except Exception as e:
            log.error(e)


class Target:
    def __init__(self, user=None, channel=None):
        self._user = user
        self._channel = channel

    @property
    def user(self):
        return self._user

    @property
    def channel(self):
        return self._channel


