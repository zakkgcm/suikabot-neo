# Events and output targets
class Event:
    def __init__(self, typ, raw=None):
        self.type = typ
        self.raw = raw

    def on_dispatch(self, client):
        pass


class MessageEvent(Event):
    def __init__(self, sender, channel, content, raw=None):
        super().__init__('message', raw)
        self.sender = sender
        self.channel = channel
        self.content = content

    def on_dispatch(self, client):
        self.sender = client.users.get_user(self.sender.name) or self.sender
