import os
import logging
import functools
import importlib.util

from collections import defaultdict

log = logging.getLogger('suika.plugin')


class Plugin:
    def __init__(self):
        self._handles = defaultdict(list)

    def event(self, event_type):
        def event_wrapper(f):
            @functools.wraps(f)
            async def handler(*args, **kwargs):
                return await f(*args, **kwargs)

            self._handles[event_type].append(handler)
            
            return handler
        return event_wrapper

    async def handle_event(self, client, event):
        propagate = True
        if event.type in self._handles:
            for handle in self._handles[event.type]:
                try:
                    propagate, event = await handle(client, event)
                except Exception as e:
                    log.error(f"Plugin threw exception handling event {event.type}: {e}")

                if not propagate:
                    break

        return propagate, event


class PluginManager:
    def __init__(self, plugin_dir='plugins/', ext='.py'):
        self._plugins = {}
        self.plugin_dir = plugin_dir
        self._ext = ext

    def find_plugins(self):
        pfiles = filter(lambda f: os.path.splitext(f)[1] == self._ext,
                        os.listdir(self.plugin_dir))

        return map(lambda f: os.path.splitext(f)[0], pfiles)

    def load(self, name):
        plugin_file = os.path.join(self.plugin_dir, name + self._ext)

        spec = importlib.util.spec_from_file_location(name, plugin_file)
        module = importlib.util.module_from_spec(spec)

        module.pid = name  # dep inject pid

        self._plugins[name] = module
        spec.loader.exec_module(module)

        log.info("Loaded plugin {0} from {1}.".format(name, plugin_file))

    def load_all(self):
        for p in self.find_plugins():
            self.load(p)

    def reload(self, name):
        # FIXME?: the owner of the manager dispatches plugin cleanup
        # is there a better way?
        del self._plugins[name]
        self.load(name)

    def reload_all(self):
        for p in list(self._plugins.keys()):
            self.reload(p)

    async def dispatch(self, client, event):
        for name, plugin in self._plugins.items():
            await plugin.p.handle_event(client, event)

    async def dispatch_one(self, client, event, pid):
        if pid in self._plugins:
            await self._plugins[pid].p.handle_event(client, event)
